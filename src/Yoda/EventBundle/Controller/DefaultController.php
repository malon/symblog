<?php

namespace Yoda\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    public function indexAction($count, $firstName)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('YodaEventBundle:Event');

        $event = $repo->findOneBy(array(
            'name' => 'Darth\'s suprise birthday party',
        ));

        return $this->render('YodaEventBundle:Default:index.html.twig', array(
            'name' => $firstName,
            'count' => $count,
            'event' => $event,
        ));
    }
}
